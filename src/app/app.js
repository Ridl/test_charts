import angular from 'angular';
import angular_chart from 'angular-chart.js';

import '../style/app.css';
import 'angular-chart.js/dist/angular-chart.css'

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor() {
      this.chartType ='Bar';
      this.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
      this.series = ['Series A', 'Serias B'];
      this.data = [
        [28, 48, 40, 19, 86, 27, 90],
        [12, 42, 30, 23, 63, 71, 14]
      ];

      this.randomize = function () {
        this.data = this.data.map(data => {
          return data.map( y => {
            y = y + Math.random() * 100 - 50;
            return parseInt(y < 0 ? 0 : y > 100 ? 100 : y);
          });
        });
      };
    this.url = 'https://github.com/preboot/angular-webpack';
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, ['chart.js'])
  .directive(MODULE_NAME, app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;
