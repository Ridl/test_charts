### Quick start

```bash
# clone our repo

git clone git@bitbucket.org:Ridl/test_charts.git

# change directory to your app
$ cd chart

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```

go to [http://localhost:8080](http://localhost:8080) in your browser.